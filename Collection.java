import java.util.ArrayList;
import java.util.List;

public class Collection
{
    private List<String> elements = new ArrayList<>();

    public Collection addElement(String element)
    {
        elements.add(element);

        return this;
    }

    public Collection removeElement(String element)
    {
        elements.remove(element);

        return this;
    }
}
